﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MainGame : MonoBehaviour {

	public Rect disallowPlatform;
    public ShopItem[] shopItems;
	public RewardItem[] rewardItems;
	public LevelData[] levelData;

    void Start()
    {
        Registry.mainGame = this;

        //Check item chances
        {
            float rchance = 0;
            foreach (RewardItem ri in rewardItems)
            {
                rchance += ri.spawnChance;
            }
			if (rchance != 1)
			{
				Debug.LogError("Reward item chances sum to " + rchance + ". Should be 1.");
			}

			float schance = 0;
			foreach (ShopItem si in shopItems)
			{
				schance += si.shopSpawnChance;
			}
			if (schance != 1)
			{
				Debug.LogError("Shop item chances sum to " + schance + ". Should be 1.");
			}
        }
    }
	
	void Update () {

		if (Input.GetMouseButtonDown (1) && Inventory.secondaryItem != null) {
			Item temp = Inventory.secondaryItem;
			Inventory.secondaryItem = Inventory.currentItem;
			Inventory.currentItem = temp;
		}

		//cheats
		if (Input.GetKeyDown (KeyCode.F10)) {
			Inventory.coins += 10;
		}
		if (Input.GetKeyDown (KeyCode.F11)) {
			Registry.player.health += 25;
		}
		if (Input.GetKeyDown (KeyCode.F12)) {
			Inventory.keys += 1;
		}
	}

	public void ResetScene () {

		Time.timeScale = 0;
		Registry.Reset ();
		Application.LoadLevel (Application.loadedLevel);
		Time.timeScale = 1;
	}
}
