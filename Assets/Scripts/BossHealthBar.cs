﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BossHealthBar : MonoBehaviour {

	public Image filling;

	void Start () {
		filling.transform.localScale = new Vector3 (1, 1, 1);
	}
	
	void Update () {
		if (Registry.boss != null) {
			Show ();
		}
	}

	public void Show () {
		if (!GetComponent<Image> ().enabled) {
			GetComponent<Image> ().enabled = true;
		}
		if (!filling.GetComponent<Image> ().enabled) {
			filling.GetComponent<Image> ().enabled = true;
		}
		filling.transform.localScale = new Vector3 (Registry.boss.health / Registry.boss.maxHealth, 1, 1);
	}
}
