﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Planet : MonoBehaviour {

	public float healthReward = 25;
	public GameObject minimapObject;
	public GameObject emptyFoliage;
	public GameObject shield;
	public float gravityFieldRadius = 4.0f;
	public float gravityStrength = 50.0f;
	public bool activeGravity = true;
	public bool evil = true;
	public int minFoliage = 5;
	public int maxFoliage = 20;
    public Sprite evilMinimap;
    public Sprite goodMinimap;
	public Sprite evilPlanet;
	public Sprite goodPlanet;
	public AudioClip planetHeal;
	public AudioClip enterEvil;
	public GameObject chestPrefab;
	bool enemiesSpawned = false;
	bool playerEntered = false;
	List<GameObject> enemies = new List<GameObject> ();
	List<GameObject> foliage = new List<GameObject> ();

	void Start () {

		Registry.planets.Add (this);

		SpawnFoliage (Random.Range (minFoliage, maxFoliage + 1));

        gameObject.GetComponent<SpriteRenderer>().sprite = evil == true ? evilPlanet : goodPlanet;

		if (Math.GetRandomChance (100)) {
			Vector3 pos = Math.GetRandomPosOnCircle (transform.localScale.x * ((CircleCollider2D)GetComponent<Collider2D>()).radius + chestPrefab.transform.position.y) + new Vector3 (transform.position.x, transform.position.y, 0);
			float angle = Mathf.Atan2 (pos.y - transform.position.y, pos.x - transform.position.x) * 180 / Mathf.PI - 90;

			Instantiate (chestPrefab, pos + new Vector3 (0, 0, chestPrefab.transform.position.z), Quaternion.Euler (0, 0, angle));
		}
	}
	
	void Update () {

		if (activeGravity) {
			foreach (Collider2D col in Physics2D.OverlapCircleAll(transform.position, gravityFieldRadius)) {
				Player player = col.GetComponent<Player> ();
				if (player != null) {

					playerEntered = true;

					if (!evil) {
						player.StartGravity (this);
					}
				}

				Enemy e = col.GetComponent<Enemy> ();
				if (e != null) {
					e.StartGravity (this);
				}
			}
		}

		if (playerEntered && enemiesSpawned && evil) {
			bool dead = true;

			foreach (GameObject go in enemies) {
				if (go != null) {
					dead = false;
					break;
				}
			}

			if (dead) {
				HealPlanet ();
			}
		}

		if (playerEntered) {
			Minimap.AddObject (minimapObject);

            if (evil)
            {
                minimapObject.GetComponent<SpriteRenderer>().sprite = evilMinimap;
            }
            else
            {
                minimapObject.GetComponent<SpriteRenderer>().sprite = goodMinimap;
            }
		}

		if (playerEntered && !enemiesSpawned && evil) {
            Registry.player.groundPlanet = this;
			LockInPlayer ();
			SpawnEnemies (Random.Range (5, 15));
		}
	}

	void FixedUpdate () {
		if (playerEntered && evil) {
			if ((Registry.player.transform.position - transform.position).sqrMagnitude >= (gravityFieldRadius + Registry.player.liftoffOffset) * (gravityFieldRadius + Registry.player.liftoffOffset)) {
				Registry.player.transform.position = (Registry.player.transform.position - transform.position).normalized * (gravityFieldRadius + Registry.player.liftoffOffset) + transform.position;
			}
		}
	}

	void SpawnFoliage (int amount) {
		Sprite[] sprites = evil == true ? Registry.mainGame.levelData [Registry.currentLevel].evilSprites : Registry.mainGame.levelData [Registry.currentLevel].normalSprites;

		for (int i = 0; i < amount; i++) {
			Vector3 pos = Math.GetRandomPosOnCircle (transform.localScale.x * ((CircleCollider2D)GetComponent<Collider2D>()).radius + emptyFoliage.transform.position.y) + new Vector3 (transform.position.x, transform.position.y, 0);
			float angle = Mathf.Atan2 (pos.y - transform.position.y, pos.x - transform.position.x) * 180 / Mathf.PI - 90;

			GameObject foliage = (GameObject)Instantiate (emptyFoliage, pos + new Vector3 (0, 0, emptyFoliage.transform.position.z), Quaternion.Euler (0, 0, angle));

			foliage.GetComponent<SpriteRenderer> ().sprite = sprites [Random.Range (0, sprites.Length)];
			this.foliage.Add (foliage);
		}
	}

	void SpawnEnemies (int amount) {
		GameObject[] eps = Registry.mainGame.levelData [Registry.currentLevel].enemyPrefabs;

		for (int i = 0; i < amount; i++) {
			Vector3 pos = Math.GetRandomPosOnCircle (transform.localScale.x * ((CircleCollider2D)GetComponent<Collider2D>()).radius + 0.5f) + new Vector3 (transform.position.x, transform.position.y, 0);
			float angle = Mathf.Atan2 (pos.y - transform.position.y, pos.x - transform.position.x) * 180 / Mathf.PI - 90;

			GameObject randEnemy = eps [Random.Range (0, eps.Length)];
			GameObject e = (GameObject)Instantiate (randEnemy, pos + new Vector3 (0, 0, randEnemy.transform.position.z), Quaternion.Euler (0, 0, angle));
			enemies.Add (e);
		}

		enemiesSpawned = true;
	}

	void HealPlanet () {

        Registry.screenFlash.Flash(Color.white);

		foreach (GameObject go in foliage) {
			Sprite s = Registry.mainGame.levelData [Registry.currentLevel].normalSprites [Registry.mainGame.levelData [Registry.currentLevel].evilSprites.ToList<Sprite> ().IndexOf (go.GetComponent<SpriteRenderer> ().sprite)];
			go.GetComponent<SpriteRenderer> ().sprite = s;
		}

		Registry.PlaySound (planetHeal);
		shield.SetActive (false);
		evil = false;

        Registry.player.health += healthReward;
        Inventory.DropRewardItem(Registry.player.transform.position, transform.rotation);

		gameObject.GetComponent<SpriteRenderer> ().sprite = goodPlanet;
        minimapObject.GetComponent<SpriteRenderer>().sprite = goodMinimap;
	}

	void LockInPlayer () {
		//Spawn Shield
		shield.SetActive (true);
		//Play Sound
		Registry.PlaySound (enterEvil);
	}
}
