﻿using UnityEngine;
using System.Collections;

public class BossPlatform : Platform {
	
	bool lockedIn = false;
	bool done = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (playerEntered && !lockedIn && !done) {
			Lock();
			GameObject boss = (GameObject) Instantiate (Registry.mainGame.levelData[Registry.currentLevel].bossPrefab, transform.position, Quaternion.identity);
			boss.GetComponent<Boss> ().bossPlatform = gameObject;
		}
	}

	public void Lock(){
		barrier.SetActive(true);
		lockedIn = true;
		done = true;

		//Play sound
	}

	public void Unlock(){
		barrier.SetActive(false);
		lockedIn = false;
		
		//Play sound
	}

}
