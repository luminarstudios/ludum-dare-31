﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

	[HideInInspector]
	public bool playerEntered = false;

    public GameObject minimapObject;
	public GameObject barrier;

	public virtual void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Player") {
			playerEntered = true;
            Minimap.AddObject (minimapObject);
		}
	}

}
