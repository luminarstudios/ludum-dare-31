using UnityEngine;
using System.Collections.Generic;

public class ShopPlatform : Platform {

	public Vector3[] itemPos;

	List<ShopItem> spawnedItems;

	bool locked = true;

	void Start () {
		spawnedItems = new List<ShopItem> ();
		if (Registry.mainGame.shopItems.Length < itemPos.Length)
			Debug.LogError (Registry.mainGame.shopItems.Length + " shop items exist, needs to be at least " + itemPos.Length);

		for (int i = 0; i < itemPos.Length; i++) {
			GameObject go;
			ShopItem si;
			if (spawnedItems.Count > 0) {
				si = Math.GetRandomShopItem ();
				bool exists = false;
				foreach (ShopItem sitem in spawnedItems) {
					if (sitem == si) {
						exists = true;
						break;
					}
				}
				if (exists) {
					i--;
					continue;
				} else {
					go = (GameObject)Instantiate (si.itemPrefab, itemPos [i] + transform.position + new Vector3(0, 0, -transform.position.z + si.itemPrefab.transform.position.z), Quaternion.identity);
				}
			} else {
				si = Math.GetRandomShopItem ();
                go = (GameObject)Instantiate(si.itemPrefab, itemPos[i] + transform.position + new Vector3(0, 0, -transform.position.z + si.itemPrefab.transform.position.z), Quaternion.identity);
			}
			spawnedItems.Add (si);

			Item item = go.GetComponent<Item>();
			if(item != null){
				item.inShop = true;
			}else{
				go.GetComponent<Pickup>().inShop = true;
			}
		}
	}
	
	void Update () {
		if (playerEntered && locked && Inventory.keys > 0) {
			locked = false;
			barrier.SetActive(false);
			Inventory.keys--;

			//Play sound
		}
	}

	public override void OnTriggerEnter2D(Collider2D col){
		if (col.tag == "Player" && Inventory.keys > 0) {
			playerEntered = true;
		} else if (col.tag == "Player") {
			Registry.keyNotice.Show();
		}
	}
}
