﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Registry {

	public static PrefabSpawner prefabSpawner;
	public static Boss boss;
    public static ScreenFlash screenFlash;
	public static KeyNotice keyNotice;
	public static bool bossDefeated = false;
	public static MainGame mainGame;
	public static GameObject shopPlatform;
	public static GameObject bossPlatform;
	public static int currentLevel = 0;
	public static Player player;
	public static float worldEnd = 180;
	public static int planetAmount = 23;
	public static List<Planet> planets = new List<Planet> ();
	public static string version = "0.4.2 alpha";
	public static string copyright = "(C) Luminar Studios 2014-2015";

	public static void Save () {

	}
	
	public static void Load () {

	}

	public static void Reset () {
		Inventory.Reset ();
		player = null;
		planets.Clear ();
	}

	public static void PlaySound (AudioClip clip) {
		AudioSource s = Camera.main.GetComponent<AudioSource> ();
		s.PlayOneShot (clip);
	}

}
