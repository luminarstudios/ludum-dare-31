﻿using UnityEngine;
using System.Collections;

public class Math {

	public static Vector3 GetRandomPosOnCircle(float radius){
		Vector2 pos = Random.insideUnitCircle;
		return pos.normalized * radius;
	}

	public static RewardItem GetRandomRewardItem(){
		float rand = Random.value;

		float temp = 0;
		foreach(RewardItem ri in Registry.mainGame.rewardItems){
			if(rand >= temp && rand < temp + ri.spawnChance){
				return ri;
			}
			temp += ri.spawnChance;
		}
		return null;
	}

	public static ShopItem GetRandomShopItem(){
		float rand = Random.value;
		
		float temp = 0;
		foreach(ShopItem si in Registry.mainGame.shopItems){
			if(rand >= temp && rand < temp + si.shopSpawnChance){
				return si;
			}
			temp += si.shopSpawnChance;
		}
		return null;
	}

	public static bool GetRandomChance(float percentage){
		float chance = percentage / 100.0f;

		float n = Random.value;
		if (n < chance) {
			return true;
		} else {
			return false;
		}
	}

}
