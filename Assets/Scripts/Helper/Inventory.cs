﻿using UnityEngine;
using System.Collections.Generic;

public class Inventory {

	public static int coins = 0;
	public static int keys = 2;
	public static int bombs = 0;
	public static int[] items = new int[5];
    public static int selected = 0;
	public static Item currentItem;
	public static Item secondaryItem;

	public static void Reset(){
		coins = 0;
		keys = 0;
		bombs = 0;
	}

    public static void DropRewardItem(Vector3 position, Quaternion rotation)
    {
        GameObject randomItemPrefab = Math.GetRandomRewardItem().itemPrefab;
        GameObject go = (GameObject) GameObject.Instantiate(randomItemPrefab, position, rotation);

        go.GetComponent<Pickup>().groundPlanet = Registry.player.groundPlanet;

        GUIManager.rewardPanel.Show(2.0f, randomItemPrefab.GetComponent<SpriteRenderer>().sprite);
    }

}
