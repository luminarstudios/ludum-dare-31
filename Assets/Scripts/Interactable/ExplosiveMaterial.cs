﻿using UnityEngine;
using System.Collections;

public class ExplosiveMaterial : MonoBehaviour {

	public float drag = 5.0f;
	public float startSpeed = 10.0f;

	void Start () {
        startSpeed += Random.Range(-0.75f, 0.75f);
	}
	
	void Update () {
		
	}

	void FixedUpdate(){
		if (startSpeed <= 0)
			return;

		transform.Translate (Vector3.up * startSpeed * Time.deltaTime);
		startSpeed -= drag * Time.deltaTime;
	}
}
