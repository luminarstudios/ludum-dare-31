﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

    public int price = 5;

    [HideInInspector]
    public bool pickedUp = false;

    [HideInInspector]
    public bool inShop = false;

	void Start () {
	
	}
	
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){
        if (col.tag == "Player" && !pickedUp)
        {
            if (inShop)
            {
                if (Inventory.coins >= price)
                {
                    Inventory.coins -= price;
					inShop = false;
                    TriggerPickup();
                }
            }
            else
            {
                TriggerPickup();
            }
        }
	}

	void OnTriggerExit2D(Collider2D col){
		if (col.tag == "Player" && this != Inventory.currentItem)
		{
			pickedUp = false;
		}
	}

	void Drop(){
		Registry.player.DetachItem (Inventory.secondaryItem.gameObject);
	}

    void TriggerPickup()
    {
		pickedUp = true;
		Registry.player.AttachItem (gameObject);

		if (Inventory.secondaryItem != null) 
			Drop ();

		Inventory.secondaryItem = Inventory.currentItem;
		Inventory.currentItem = this;
		OnPickup ();
	}

	public virtual void OnPickup(){

	}
}
