﻿using UnityEngine;
using System.Collections;

public class Coin : Pickup {

	public override void Collect(){

		Registry.PlaySound (pickupSound);
		Inventory.coins += value;
		Destroy (gameObject);
	
	}
}
