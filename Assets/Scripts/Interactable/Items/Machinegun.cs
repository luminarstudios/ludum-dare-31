﻿using UnityEngine;
using System.Collections;

public class Machinegun : Item {
	
	public GameObject bulletPrefab;
	public float pushbackStrength = 5;
	public AudioClip shoot;

	public float scatterAngle = 1;
	public float timeBetweenShots = 0.5f;
	float time = 0;
	
	void Start () {
		
	}
	
	void Update () {
		time += Time.deltaTime;
		
		if (Time.timeScale != 0 && this == Inventory.currentItem)
		{
			if (Input.GetButton("Fire1") && time >= timeBetweenShots)
			{
				time = 0;
				Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
				
				GameObject b = Instantiate(bulletPrefab, Registry.player.transform.position, Quaternion.identity) as GameObject;
				b.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(transform.position.y - mousePosition.y, transform.position.x - mousePosition.x) * 180 / Mathf.PI + Random.Range(-scatterAngle,scatterAngle));
				
				Registry.player.GetComponent<Rigidbody2D>().AddForce(-(new Vector2(mousePosition.x, mousePosition.y) - Registry.player.GetComponent<Rigidbody2D>().position).normalized * pushbackStrength);
				
				Registry.PlaySound(shoot);
				
			}
			
		}
	}
}
