﻿using UnityEngine;
using System.Collections;

public class Shotgun : Item {

    public GameObject bulletPrefab;
    public float pushbackStrength = 10;
    public AudioClip shoot;
    public float sprayAngle = 10.0f;
	public float shotAmount = 3;

    public float timeBetweenShots = 1f;
    float time = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;

        if (Time.timeScale != 0 && this == Inventory.currentItem)
        {
            if (Input.GetButtonDown("Fire1") && time >= timeBetweenShots)
            {
                time = 0;
                Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                float angle = Mathf.Atan2(transform.position.y - mousePosition.y, transform.position.x - mousePosition.x) * 180 / Mathf.PI - (sprayAngle / 2);

				float indAng = sprayAngle / (shotAmount - 1);

				for(int i = 0; i < shotAmount; i++){
					GameObject b1 = Instantiate(bulletPrefab, Registry.player.transform.position, Quaternion.identity) as GameObject;
					b1.transform.rotation = Quaternion.Euler(0, 0, angle + (i * indAng));
				}

                Registry.player.GetComponent<Rigidbody2D>().AddForce(-(new Vector2(mousePosition.x, mousePosition.y) - Registry.player.GetComponent<Rigidbody2D>().position).normalized * pushbackStrength);

                Registry.PlaySound(shoot);

            }

        }
    }
}
