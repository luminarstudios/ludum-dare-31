﻿using UnityEngine;
using System.Collections;

public class NormalGun : Item {

	public bool singleShot = true;
    public GameObject shotTemplate;
    public float pushbackStrength = 5;
    public AudioClip shoot;

    public float timeBetweenShots = 0.5f;
    float time = 0;

	bool shooting = false;

	void Start () {
	
	}
	
	void Update () {
        time += Time.deltaTime;

        if (Time.timeScale != 0 && this == Inventory.currentItem) {

			if (singleShot) {
				shooting = Input.GetButtonDown ("Fire1");
			} else {
				shooting = Input.GetButton ("Fire1");
			}

			if (shooting && time > timeBetweenShots) {
				time = 0;
				Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);

				GameObject b = Instantiate (shotTemplate, Registry.player.transform.position, Quaternion.identity) as GameObject;
				b.transform.rotation = Quaternion.Euler (0, 0, Mathf.Atan2 (transform.position.y - mousePosition.y, transform.position.x - mousePosition.x) * 180 / Mathf.PI + 90);

				b.transform.DetachChildren ();
				Destroy (b);

				Registry.player.GetComponent<Rigidbody2D>().AddForce (-(new Vector2 (mousePosition.x, mousePosition.y) - Registry.player.GetComponent<Rigidbody2D>().position).normalized * pushbackStrength);

				Registry.PlaySound (shoot);
			}
            

		}
	}
}
