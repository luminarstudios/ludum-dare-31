﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

    public float explosionDamage = 1.0f;
    public float triggerTime = 0.1f;
    float triggerTimer = 0;

    public float lifeTime = 1.0f;
    float lifeTimer = 0;

    public float radius = 1.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        triggerTimer += Time.deltaTime;
        if (triggerTimer >= triggerTime)
        {
            triggerTimer = -lifeTime;
            
			foreach(Collider2D c in Physics2D.OverlapCircleAll(transform.position, radius)){
				ExplosiveMaterial m = c.GetComponent<ExplosiveMaterial>();

				if(m != null){
					Instantiate(Registry.prefabSpawner.explosionPrefab, c.transform.position, Quaternion.identity);
					Destroy(c.gameObject);
				}

				Bullet b = c.GetComponent<Bullet>();

				if(b != null && !b.player){
					Instantiate(Registry.prefabSpawner.explosionPrefab, c.transform.position, Quaternion.identity);
					Destroy(c.gameObject);
				}

                Enemy e = c.GetComponent<Enemy>();

                if (e != null)
                {
                    e.health -= explosionDamage;
                }
			}
        }

        lifeTimer += Time.deltaTime;
        if (lifeTimer >= lifeTime)
        {
            Destroy(gameObject);
        }
	}
}
