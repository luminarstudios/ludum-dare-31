﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

    [HideInInspector]
    public Planet groundPlanet;

	public int value = 1;
	public int price = 1;

	public AudioClip pickupSound;

	[HideInInspector]
	public bool inShop = false;

	public void Take(){
		if (inShop) {
			if(Inventory.coins >= price){
				Inventory.coins -= price;
				inShop = false;
				Collect();
			}
		} else {
			Collect();
		}
	}

	public virtual void Collect(){
        
	}
}
