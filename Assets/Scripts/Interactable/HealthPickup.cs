﻿using UnityEngine;
using System.Collections;

public class HealthPickup : Pickup {

    float currentSpeed = 0;

    void Update()
    {
        if (!inShop)
        {
            transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2(groundPlanet.transform.position.y - transform.position.y, groundPlanet.transform.position.x - transform.position.x) * 180 / Mathf.PI + 90);

            float minDist = groundPlanet.transform.localScale.x * ((CircleCollider2D)groundPlanet.GetComponent<Collider2D>()).radius + ((BoxCollider2D)GetComponent<Collider2D>()).size.y / 2;
            if ((groundPlanet.transform.position - transform.position).sqrMagnitude > minDist * minDist)
            {
                currentSpeed += -Physics2D.gravity.y * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, groundPlanet.transform.position, currentSpeed * Time.deltaTime);
            }
            else
            {
                currentSpeed = 0;
            }
        }
    }

	public override void Collect(){
		if (Registry.player.health < Registry.player.maxHealth) {
			Registry.PlaySound (pickupSound);
			Registry.player.health += value;
			Destroy (gameObject);
		}
	}
}
