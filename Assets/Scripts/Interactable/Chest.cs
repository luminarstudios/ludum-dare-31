﻿using UnityEngine;
using System.Collections;

public class Chest : MonoBehaviour {

	public GameObject chestLid;
	bool opened = false;

	void Start () {
	
	}
	
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col){
		if (Inventory.keys > 0 && !opened && col.gameObject == Registry.player.gameObject) {
			Open ();
			Inventory.keys--;
			opened = true;
		} else if (col.gameObject == Registry.player.gameObject && !opened) {
			Registry.keyNotice.Show ();
		}
	}

	void Open(){
		//Remove lid
		chestLid.SetActive (false);

		//Play sound

		//Spawn item
		SpawnItem ();
	}

	void SpawnItem(){
		Inventory.DropRewardItem(Registry.player.transform.position, transform.rotation);
	}
}
