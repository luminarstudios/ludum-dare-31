﻿using UnityEngine;
using System.Collections;

public class Key : Pickup {

	public override void Collect(){
		
		Registry.PlaySound (pickupSound);
		Inventory.keys += value;
		Destroy (gameObject);
		
	}
}
