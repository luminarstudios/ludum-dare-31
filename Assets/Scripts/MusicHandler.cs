﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.IO;

public class MusicHandler : MonoBehaviour {

	public static List<AudioClip> music;
	AudioSource audioSource;

	int playing = 0;

	// Use this for initialization
	void Start () {
		music = new List<AudioClip>();

		audioSource = Camera.main.gameObject.GetComponent<AudioSource> ();

		DirectoryInfo musicDir = Directory.CreateDirectory ("music");
		FileInfo[] files = musicDir.GetFiles ();

		if (files.Length > 0) {
			for(int i = 0; i < files.Length; i++){
				if(files[i].Extension == ".wav" || files[i].Extension == ".ogg"){
					StartCoroutine(GetMusicClip(files[i].FullName));
				}
			}
		}
	}

	IEnumerator GetMusicClip(string path){
		WWW www = new WWW("file://" + path.Replace('\\', '/'));

		yield return www;

		AudioClip ac = www.audioClip;
		music.Add (ac);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.N)) {
			AdvanceTrack();
		}

		if (!audioSource.isPlaying && music.Count > 0) {
			AdvanceTrack();
		}
	}

	void AdvanceTrack(){
		playing++;
		if(playing >= music.Count){
			playing = 0;
		}
		
		audioSource.clip = music[playing];
		audioSource.Play();
	}
}
