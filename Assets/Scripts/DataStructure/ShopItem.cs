﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ShopItem {

	public GameObject itemPrefab;
	public float shopSpawnChance;

}
