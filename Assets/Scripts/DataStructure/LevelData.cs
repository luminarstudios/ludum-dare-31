﻿using UnityEngine;
using System.Collections.Generic;

[System.Serializable]
public class LevelData {

	public GameObject planetPrefab;
	public GameObject bossPlatformPrefab;
	public GameObject shopPlatformPrefab;
	public GameObject bossPrefab;
	public GameObject[] enemyPrefabs;
	public Sprite[] evilSprites;
	public Color evilColor;
	public Sprite[] normalSprites;
	public Color normalColor;

}
