﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class RewardItem {

	public GameObject itemPrefab;
	public float spawnChance;

}
