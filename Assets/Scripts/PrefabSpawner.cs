﻿using UnityEngine;
using System.Collections;

public class PrefabSpawner : MonoBehaviour {

	public GameObject playerPrefab;
	public GameObject blackHolePrefab;
	public GameObject explosionPrefab;

	int normalCount = 5;

	void Start () {

		Registry.prefabSpawner = this;

		if (Registry.player == null) {
			GameObject.Instantiate (playerPrefab, Vector3.zero, Quaternion.identity);
		} else {
			Registry.player.transform.position = Vector3.zero;
		}

		SpawnShopPlatform ();
		SpawnBossPlatform ();

		for (int i = 0; i < Registry.planetAmount; i++) {
			SpawnPlanet();
		}
	
	}
	
	void Update () {
	
	}

	void SpawnShopPlatform(){
		GameObject shopPrefab = Registry.mainGame.levelData [Registry.currentLevel].shopPlatformPrefab;

		float xDist = ((BoxCollider2D)shopPrefab.GetComponent<Collider2D>()).size.x + 10;
		float yDist = ((BoxCollider2D)shopPrefab.GetComponent<Collider2D>()).size.y + 10;
		Vector2 pos = new Vector2(Random.Range(-Registry.worldEnd + xDist,Registry.worldEnd - xDist), Random.Range(-Registry.worldEnd + yDist,Registry.worldEnd - yDist));

		if (Registry.mainGame.disallowPlatform.Contains (pos)) {
			SpawnShopPlatform ();
		} else {
			GameObject go = (GameObject) Instantiate(shopPrefab, ((Vector3)pos) + new Vector3(0, 0, shopPrefab.transform.position.z), shopPrefab.transform.rotation);
			Registry.shopPlatform = go;
		}
	}

	void SpawnBossPlatform(){
		GameObject bossPlatformPrefab = Registry.mainGame.levelData [Registry.currentLevel].bossPlatformPrefab;
		
		float xDist = ((BoxCollider2D)bossPlatformPrefab.GetComponent<Collider2D>()).size.x + 10;
		float yDist = ((BoxCollider2D)bossPlatformPrefab.GetComponent<Collider2D>()).size.y + 10;
		Vector2 pos = new Vector2(Random.Range(-Registry.worldEnd + xDist,Registry.worldEnd - xDist), Random.Range(-Registry.worldEnd + yDist,Registry.worldEnd - yDist));
		
		if (Registry.mainGame.disallowPlatform.Contains (pos) || (((Vector3)pos) - Registry.shopPlatform.transform.position).sqrMagnitude < xDist * xDist + yDist * yDist) {
			SpawnBossPlatform ();
		} else {
			GameObject go = (GameObject) Instantiate(bossPlatformPrefab, ((Vector3)pos) + new Vector3(0, 0, bossPlatformPrefab.transform.position.z), bossPlatformPrefab.transform.rotation);
			Registry.bossPlatform = go;
		}
	}

	void SpawnPlanet(){
		GameObject planetPrefab = Registry.mainGame.levelData [Registry.currentLevel].planetPrefab;

		float nDist = planetPrefab.GetComponent<Planet>().gravityFieldRadius;
		Vector2 pos = new Vector2(Random.Range(-Registry.worldEnd + nDist,Registry.worldEnd - nDist), Random.Range(-Registry.worldEnd + nDist,Registry.worldEnd - nDist));

		bool tooClose = false;
		if(Physics2D.OverlapCircleAll(pos, planetPrefab.GetComponent<Planet>().gravityFieldRadius * 2).Length > 0){
			tooClose = true;
		}

		if (tooClose) {
			SpawnPlanet ();
		} else {
			GameObject planet = (GameObject) Instantiate (planetPrefab, new Vector3 (pos.x, pos.y, 0), planetPrefab.transform.rotation);

			if(normalCount > 0){
				planet.GetComponent<Planet>().evil = false;
				normalCount--;
			}
		}
	}
}
