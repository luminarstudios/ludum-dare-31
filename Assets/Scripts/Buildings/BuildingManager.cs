﻿using UnityEngine;
using System.Collections;

public class BuildingManager : MonoBehaviour {

	public AudioClip buildSound;

	public GameObject[] buildingPrefabs;

	public void Build (int building) {
		if (Registry.player.canBuild && Time.timeScale != 0) {
			Building buildingPrefab = buildingPrefabs [building].GetComponent<Building> ();
			if (Inventory.coins >= buildingPrefab.cost && buildingPrefab.canBeBuilt) {
				Inventory.coins -= buildingPrefab.cost;
				GameObject b = Instantiate (buildingPrefabs [building], Registry.player.transform.position + new Vector3 (0, 0, -Registry.player.transform.position.z + buildingPrefab.transform.position.z), Registry.player.transform.rotation) as GameObject;
				b.GetComponent<Building> ().buildingPlanet = Registry.player.groundPlanet;
				b.GetComponent<Building> ().Init ();
				Registry.PlaySound(buildSound);
			}
		}
	}
}
