﻿using UnityEngine;
using System.Collections;

public class Building : MonoBehaviour {

	public float timer = 10.0f;
	protected float passedTime = 0.0f;
	public int cost = 5;

	public virtual bool canBeBuilt {
		get {
			return true;
		}
	}

	protected Planet planet;
	public Planet buildingPlanet {
		get {
			return planet;
		}
		set {
			planet = value;
		}
	}

	public virtual void Init(){

	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
