﻿using UnityEngine;
using System.Collections;

public class OilDrill : Building {

	public GameObject gasPrefab;
	GameObject currentGas;

	/*public override bool canBeBuilt {
		get {
			Planet p = Registry.player.GetGroundPlanet ();
			if (p != null) {
				if (p.oil > 0 && p.availableOilDrills > 0) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
	}*/


	void Start () {
	
	}
	
	void Update () {

		if (planet == null) {
			Destroy(gameObject);
		}

		passedTime += Time.deltaTime;
		if (passedTime >= timer) {
			if (currentGas == null) {
				passedTime = 0.0f;
				currentGas = Instantiate (gasPrefab, transform.position + new Vector3 (0, 0, -transform.position.z + gasPrefab.transform.position.z), transform.rotation) as GameObject;
			}
		}
	}
}
