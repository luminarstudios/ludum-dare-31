﻿using UnityEngine;
using System.Collections;

public class KeyNotice : MonoBehaviour {

	public GameObject keyNotice;
	static float timer = 0;
	float lifeTime = 1;
	
	void Start () {
		Registry.keyNotice = this;
	}
	
	void Update () {
		timer += Time.deltaTime;
		if (timer >= lifeTime){
			keyNotice.SetActive(false);
		}
	}
	
	public void Show() {
		timer = 0;
		keyNotice.SetActive(true);
	}
}