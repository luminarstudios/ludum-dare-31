﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GunPanel : MonoBehaviour {

	public GameObject currentGunPanel;
	public GameObject otherGunPanel;

	public Image currentGun;
	public Image otherGun;

	void Start () {
	}
	
	void Update () {
		if (Inventory.currentItem) {
			currentGunPanel.SetActive(true);
			currentGun.sprite = Inventory.currentItem.GetComponent<SpriteRenderer>().sprite;
		}
		if (Inventory.secondaryItem) {
			otherGunPanel.SetActive(true);
			otherGun.sprite = Inventory.secondaryItem.GetComponent<SpriteRenderer>().sprite;
		}
	}
}
