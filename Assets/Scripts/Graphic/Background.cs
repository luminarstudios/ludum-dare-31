﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	public float parallax = 4.0f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

		if (Registry.player != null) {
			Vector2 offset = Vector2.zero;

			offset.x = Registry.player.transform.position.x / transform.localScale.x / parallax * GetComponent<Renderer>().material.mainTextureScale.x;
			offset.y = Registry.player.transform.position.y / transform.localScale.y / parallax * GetComponent<Renderer>().material.mainTextureScale.x;

			GetComponent<Renderer>().material.mainTextureOffset = offset;
		}

	}
}
