using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIManager : MonoBehaviour {

    public static RewardPanel rewardPanel;
	public Text coinDisplay;
	public Text keys;
	public Text health;
	public Text diamonds;
	public Text fps;
	public GameObject exitPanel;
	public GameObject settingsPanel;
	public GameObject winText;
	public GameObject loseText;
	public GameObject instructions;

	public float timer = 0;
	public float maxTime = 0.5f;

	void Start () {
		fps.text = "" + Mathf.Round (1 / Time.deltaTime);
		if (Registry.currentLevel == 0)
			instructions.SetActive (true);
		Time.timeScale = 1;
	}
	
	void Update () {
		if (Input.GetKeyDown (KeyCode.F))
			fps.gameObject.SetActive(!fps.gameObject.activeInHierarchy);
		timer += Time.deltaTime;
		if (timer >= maxTime && fps.gameObject.activeInHierarchy) {
			fps.text = "" + Mathf.Round (1 / Time.deltaTime);
			timer = 0.5f;
		}
		keys.text = "" + Inventory.keys;
		if (Registry.player != null) {
			/*if (Registry.diamonds >= Registry.diamondAmount) {
				winText.SetActive (true);
				Time.timeScale = 0;	
			}*/

			if (Registry.player != null) {
				if (Registry.player.health <= 0)
					loseText.SetActive (true);
			}

			if (Input.GetKeyDown (KeyCode.Escape)) {
				if (exitPanel.activeSelf)
					Resume ();
				else {
					exitPanel.SetActive (true);
					Time.timeScale = 0;
					if (settingsPanel.activeSelf)
						settingsPanel.SetActive (false);
				}
			}
	
			coinDisplay.text = "" + Inventory.coins;
			health.text = "" + Mathf.Round (Registry.player.health);
			//diamonds.text = "" + Registry.diamonds;
			//jetpackFuel.text = "" + Mathf.Round (Registry.jetpackFuel * 100) / 100;
		}
	}

	public void Settings () {
		exitPanel.SetActive (false);
		gameObject.GetComponent<Settings> ().SetWindowedToggle ();
		settingsPanel.SetActive (true);
	}

	public void HideSettings () {
		settingsPanel.SetActive (false);
		exitPanel.SetActive (true);
	}

	public void ExitGame () {
		Time.timeScale = 0;
		exitPanel.SetActive (false);
		Application.Quit ();
	}

	public void ExitToMainMenu () {
		Application.LoadLevel ("MainMenu");
	}

	public void Resume () {
		exitPanel.SetActive (false);
		if (Registry.player.health > 0)
			Time.timeScale = 1;
	}
}
