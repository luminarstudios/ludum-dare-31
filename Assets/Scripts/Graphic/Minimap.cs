﻿using UnityEngine;
using System.Collections.Generic;

public class Minimap : MonoBehaviour {

    // NOTE(ADOMAS): Optimize minimap

	public float width = 250;
	public float height = 250;
	public float minScale = 25;
	Camera cam;
	public static List<GameObject> minimapObjects = new List<GameObject> ();

    static GameObject highest;
    static GameObject lowest;
    static GameObject leftest;
    static GameObject rightest;

	void Start () {
		cam = GetComponent<Camera> ();

		Rect rect = new Rect ();

		rect.width = width / Screen.width;
		rect.height = height / Screen.height;
		rect.x = 1 - rect.width;
		rect.y = 1 - rect.height;

		cam.rect = rect;
	}
	
	void Update () {

        if (Input.GetKeyDown(KeyCode.M)) gameObject.GetComponent<Camera>().enabled = !gameObject.GetComponent<Camera>().enabled;

        if (Registry.player == null)
            return;

		Vector3 middle = Vector3.zero;

		middle.x = (leftest.transform.position.x + rightest.transform.position.x) / 2;
        middle.y = (highest.transform.position.y + lowest.transform.position.y) / 2;

        middle.x = (middle.x + Registry.player.transform.position.x) / 2;
        middle.y = (middle.y + Registry.player.transform.position.y) / 2;

		middle.z = transform.position.z;
		Vector3 newPos = Vector3.MoveTowards (transform.position, middle, 100 * Time.deltaTime);
		transform.position = newPos;

		if (Mathf.Abs (highest.transform.position.y - middle.y) > Mathf.Abs (rightest.transform.position.x - middle.x)) {
            cam.orthographicSize = Mathf.Abs(highest.transform.position.y - middle.y) + 15;
		} else {
            cam.orthographicSize = Mathf.Abs(rightest.transform.position.x - middle.x) + 15;
		}

		if (cam.orthographicSize < minScale) {
			cam.orthographicSize = minScale;
		}

	}

    public static void AddObject(GameObject obj)
    {
        minimapObjects.Add(obj);

        if (highest == null)
        {
            highest = obj;
        }
        else if (obj.transform.position.y > highest.transform.position.y)
        {
            highest = obj;
        }

        if (lowest == null)
        {
            lowest = obj;
        }
        else if (obj.transform.position.y < lowest.transform.position.y)
        {
            lowest = obj;
        }

        if (rightest == null)
        {
            rightest = obj;
        }
        else if (obj.transform.position.x > rightest.transform.position.x)
        {
            rightest = obj;
        }

        if (leftest == null)
        {
            leftest = obj;
        }
        else if (obj.transform.position.x < leftest.transform.position.x)
        {
            leftest = obj;
        }
    }
}
