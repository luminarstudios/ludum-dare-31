﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour {

	public Planet planet;

	void Start () {

	}
	
	void Update () {

		if (!Camera.main.pixelRect.Contains(Camera.main.WorldToScreenPoint(planet.transform.position))) {
	
			float x = (Camera.main.orthographicSize - 0.5f) / (planet.transform.position - Registry.player.transform.position).magnitude;

			transform.position = Vector3.Lerp (Registry.player.transform.position, planet.transform.position, x);

			float dx = transform.position.x - planet.transform.position.x;
			float dy = transform.position.y - planet.transform.position.y;
			
			float a = Mathf.Atan2 (dy, dx) * 180 / Mathf.PI + 90;
			transform.rotation = Quaternion.Euler (0, 0, a);

			Color c = GetComponent<SpriteRenderer>().color;
			c.a = 1;
			GetComponent<SpriteRenderer>().color = c;
		} else {
			Color c = GetComponent<SpriteRenderer>().color;
			c.a = 0;
			GetComponent<SpriteRenderer>().color = c;
		}
	}
}
