﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainMenu : MonoBehaviour {

	public Text version;
	public Text copyright;
	public GameObject settingsPanel;
	public GameObject menuPanel;


	void Start () {
		version.text = "v" + Registry.version;
		copyright.text = Registry.copyright;
		Registry.Reset ();
	}
	
	void Update () {
	
		if (Input.GetKeyDown (KeyCode.Escape))
			HideSettings ();

	}

	public void Settings () {
		menuPanel.SetActive (false);
		gameObject.GetComponent<Settings> ().SetWindowedToggle ();
		settingsPanel.SetActive (true);
	}
	
	public void HideSettings () {
		settingsPanel.SetActive (false);
		menuPanel.SetActive (true);
	}

	public void ExitGame () {
		Application.Quit ();
	}

	public void StartGame () {
		Application.LoadLevel("scene01");
	}
}
