﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenFlash : MonoBehaviour {

	public Image flashImage;
	public float time = 0.1f;
	public float fadeOutTime = 0.2f;
	float timer = 0;
	float alpha = 0;
	bool started = false;

	void Start () {
		Registry.screenFlash = this;
	}
	
	void Update () {
		if (started) {
			timer += Time.deltaTime;

			if (timer > time && timer < time + fadeOutTime) {
				alpha = (fadeOutTime - (timer - time)) / fadeOutTime;
			}

			if (timer > time + fadeOutTime) {
				started = false;
			}

		} else {
			alpha = 0;
		}

		flashImage.color = new Color (flashImage.color.r, flashImage.color.g, flashImage.color.b, alpha);
	}

	public void Flash (Color color) {
		flashImage.color = color;
		timer = 0;
		alpha = 1;
		started = true;
	}
}
