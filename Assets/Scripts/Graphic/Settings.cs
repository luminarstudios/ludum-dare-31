﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Settings : MonoBehaviour {

	public int AA = 4;
	public int ResX = 1280;
	public int ResY = 720;
	public Toggle V_Sync;
	public Toggle windowed;
	public Slider resSlider;
	public Slider aaSlider;
	public bool toggled = false;
	public bool isitFullscreen;
	public Text resDisp;
	public Text aaDisp;
	string[] res = {"640x480","800x600","1024x768","1280x720","1152x864","1280x960","1440x1080","1600x900","1600x1200","1920x1080"};
	int[] aa = {0,2,4,8};

	void Start () {
		isitFullscreen = true;
		windowed.isOn = false;
		resSlider.maxValue = res.Length - 1;
	}

	public void ChangeResolution () {
		string n = res [(int)resSlider.value];
		resDisp.text = n;
		string[] ns = n.Split ('x');
		ResX = System.Int32.Parse (ns [0]);
		ResY = System.Int32.Parse (ns [1]);
	}

	public void SetAA () {
		AA = aa [(int)aaSlider.value];
		aaDisp.text = AA + "x Anti-Aliasing";	
	}

	public void Apply () {

		QualitySettings.antiAliasing = AA;
		gameObject.GetComponent<GUIManager> ().HideSettings ();
		Screen.SetResolution (ResX, ResY, isitFullscreen);


		if (V_Sync == true)
			QualitySettings.vSyncCount = 1;
		else
			QualitySettings.vSyncCount = 0;

	}

	public void Cancel () {
		gameObject.GetComponent<GUIManager> ().HideSettings ();
	}

	public void MenuCancel () {
		gameObject.GetComponent<MainMenu> ().HideSettings ();
	}

	public void MenuApply () {

		QualitySettings.antiAliasing = AA;
		gameObject.GetComponent<MainMenu> ().HideSettings ();
		Screen.SetResolution (ResX, ResY, isitFullscreen);

		if (V_Sync == true)
			QualitySettings.vSyncCount = 1;
		else
			QualitySettings.vSyncCount = 0;	
	}

	public void ToggleFullscreen () {
		toggled = !toggled;
		if (toggled) {
			isitFullscreen = false;
		} else {
			isitFullscreen = true;
		}

	}

	public void SetWindowedToggle () {
		if (Screen.fullScreen == true) {
			windowed.isOn = false;
			isitFullscreen = true;
		} else {
			windowed.isOn = true;
			isitFullscreen = false;	
		}
	}

	public void BackToMenu () {
		Application.LoadLevel ("MainMenu");
	}
}
