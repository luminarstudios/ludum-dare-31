﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RewardPanel : MonoBehaviour {

    public GameObject panelImage;
    public GameObject panelText;

    float timer = 0;
    float lifeTime = 0;

	// Use this for initialization
	void Start () {
        GUIManager.rewardPanel = this;
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer >= lifeTime)
        {
            panelImage.SetActive(false);
            panelText.SetActive(false);
        }
	}

    public void Show(float time, Sprite sprite)
    {
        lifeTime = time;
        timer = 0;
        panelImage.GetComponent<Image>().sprite = sprite;
        panelImage.SetActive(true);
        panelText.SetActive(true);
    }
}
