﻿using UnityEngine;
using System.Collections;

public class Mob : MonoBehaviour {

	public float movementSpeed = 20.0f;
	public float rotationSpeed = 0.5f;
	public float health = 100.0f;
	public float maxHealth = 100.0f;

    [HideInInspector]
	public Planet groundPlanet;

	protected bool inAtmosphere = false;
	protected bool onGround = false;
	protected bool liftingOff = false;

	void Start () {
	
	}
	
	void Update () {
	
	}

	public void StartGravity(Planet planet){
		inAtmosphere = true;
		groundPlanet = planet;
	}
}
