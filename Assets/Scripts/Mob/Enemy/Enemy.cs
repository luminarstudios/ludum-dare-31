﻿using UnityEngine;
using System.Collections.Generic;

public class Enemy : Mob
{
    [HideInInspector]
    public bool idle = true;
    [HideInInspector]
    public bool attacking = false;
    [HideInInspector]
    public bool patroling = false;

	public GameObject bulletPrefab;
	public float timeBetweenShots = 0.1f;
    public bool explodesOnImpact = false;
    public float closestDistance = 1.5f;

	float timer = 0;

	void Start () {

	}

    void Update()
    {
        if (health <= 0)
        {
            Destroy(gameObject);
        }
    }

	void FixedUpdate () {
        if (Registry.player == null)
            return;

        GetComponent<Rigidbody2D>().MoveRotation(Mathf.Atan2(Registry.player.transform.position.y - transform.position.y, Registry.player.transform.position.x - transform.position.x) * 180 / Mathf.PI - 90);
        MoveForward();

        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position, transform.up);
        if (hits.Length < 2)
            return;

        RaycastHit2D hit = hits[1];

		if (bulletPrefab != null && hit.collider.gameObject.name != "Planet(Clone)") {
			timer += Time.deltaTime;
			if (timer >= timeBetweenShots) {
				Instantiate (bulletPrefab, transform.position, Quaternion.Euler(0, 0, GetComponent<Rigidbody2D>().rotation - 90));
				timer = 0;
			}
		}
	}

    void MoveForward()
    {
        if (!explodesOnImpact)
        {
            if ((Registry.player.transform.position - transform.position).sqrMagnitude < closestDistance * closestDistance)
                return;
        }

        GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * movementSpeed);
    }

}
