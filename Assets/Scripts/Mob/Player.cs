﻿using UnityEngine;
using System.Collections;

public class Player : Mob {
	
	public ParticleSystem jpParticles;
	public ParticleSystem healingParticles;
	public float liftoffOffset = 0.5f;
	public float planetMovementSpeed = 90.0f;

	float jpParticleSpeed = 0;

	public bool canBuild {
		get {
			if (onGround && inAtmosphere && !liftingOff) {
				return true;
			} else {
				return false;
			}
		}
	}

	void Start () {
		DontDestroyOnLoad (gameObject);

		jpParticleSpeed = jpParticles.startSpeed;
		Registry.player = this;

		Minimap.AddObject (gameObject);
	}
	
	void Update () {
		if(Application.loadedLevelName != "scene01"){
			Destroy(gameObject);
		}

		Rect world = new Rect (-Registry.worldEnd - (Registry.worldEnd / 90 * 10), -Registry.worldEnd - (Registry.worldEnd / 90 * 10), (Registry.worldEnd + (Registry.worldEnd / 90 * 10)) * 2, (Registry.worldEnd + (Registry.worldEnd / 90 * 10)) * 2);

		if (!world.Contains (GetComponent<Rigidbody2D>().position)) {
			GetComponent<Rigidbody2D>().AddForce(-GetComponent<Rigidbody2D>().position);
		}

		Camera.main.transform.position = transform.position + new Vector3 (0, 0, -10);
		if (inAtmosphere && (transform.position - groundPlanet.transform.position).sqrMagnitude > (groundPlanet.gravityFieldRadius + liftoffOffset) * (groundPlanet.gravityFieldRadius + liftoffOffset)) {
			groundPlanet = null;
			inAtmosphere = false;
			liftingOff = false;
		}

		healingParticles.enableEmission = false;

		if (health <= 0) {
			health = 0;
			Time.timeScale = 0;
		}

		if (health > maxHealth) {
			health = maxHealth;
		}

		/*if (onGround && inAtmosphere && !liftingOff && groundPlanet != null) {
			// Healing
			if (health < maxHealth) {
				if (groundPlanet.healingWells > 0) {
					healingParticles.enableEmission = true;
				}
				health += groundPlanet.baseHealValue * groundPlanet.healingWells * Time.deltaTime;
			}
		}*/

		if (!inAtmosphere) {
			if (Input.GetAxis ("Vertical") != 0) {
				jpParticles.enableEmission = true;
				jpParticles.startSpeed = jpParticleSpeed * Input.GetAxis("Vertical");
			} else {
				jpParticles.enableEmission = false;
			}
		}
		if (liftingOff) {
			jpParticles.startSpeed = jpParticleSpeed * Input.GetAxis("Vertical");
			jpParticles.enableEmission = true;
		}
		if (onGround) {
			jpParticles.enableEmission = false;
		}

		if (!liftingOff && inAtmosphere) {
			if (Input.GetButtonDown ("Vertical")) {
				if (Input.GetAxis ("Vertical") > 0) {
					jpParticles.enableEmission = false;
					liftingOff = true;
				}
			}
		}
	}

	void FixedUpdate () {

		if (liftingOff) {
			GetComponent<Rigidbody2D>().AddRelativeForce (Vector2.up * movementSpeed);
		}

		if (onGround && inAtmosphere && !liftingOff) {
			transform.RotateAround (groundPlanet.transform.position, Vector3.forward, planetMovementSpeed * -Input.GetAxis ("Horizontal") * Time.deltaTime);
		}

		if (inAtmosphere) {
			GetComponent<Rigidbody2D>().rotation = Mathf.Atan2 (transform.position.y - groundPlanet.transform.position.y, transform.position.x - groundPlanet.transform.position.x) * 180 / Mathf.PI - 90;
		}

		if (inAtmosphere && !onGround && !liftingOff) {
			GetComponent<Rigidbody2D>().AddRelativeForce (-Vector2.up * groundPlanet.gravityStrength);
		}
		
		if (!inAtmosphere) {
            Vector2 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            GetComponent<Rigidbody2D>().rotation = Mathf.Atan2(transform.position.y - mouseWorldPos.y, transform.position.x - mouseWorldPos.x) * 180 / Mathf.PI + 90;

			//rigidbody2D.AddTorque (-rotationSpeed * Input.GetAxis ("Horizontal"));

            Vector2 movement = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * movementSpeed;
            
			GetComponent<Rigidbody2D>().AddRelativeForce (Vector2.ClampMagnitude(movement, movementSpeed));
		}
	}

	void OnCollisionEnter2D (Collision2D col) {
		if (col.gameObject.tag == "Planet") {
			onGround = true;
		}
	}

	void OnCollisionExit2D (Collision2D col) {
		if (col.gameObject.tag == "Planet") {
			onGround = false;
		}
	}

	void OnTriggerEnter2D (Collider2D other) {
		Pickup pickup = other.GetComponent<Pickup> ();
		if (pickup != null) {
			pickup.Take ();
		}
	}

    public void AttachItem(GameObject itemObject)
    {

        itemObject.transform.parent = transform;
        itemObject.transform.localPosition = new Vector3(0, 0, transform.localPosition.z);
		itemObject.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 0);

    }

	public void DetachItem(GameObject itemObject){

		itemObject.transform.parent = null;
		itemObject.GetComponent<SpriteRenderer> ().color = new Color (1, 1, 1, 1);
		itemObject.transform.rotation = Quaternion.identity;

	}
}
