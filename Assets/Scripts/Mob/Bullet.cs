﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float bulletSpeed = 2.0f;
	public float damage = 50.0f;
	public float timeout = 5.0f;
	[HideInInspector]
	public bool stopped = false;
	float timer = 0;

	public bool player = true;

	void Update () {
		timer += Time.deltaTime;
		if (timer >= timeout) {
			Destroy (gameObject);
		}
		if (!stopped) {
			transform.Translate (Vector3.left * Time.deltaTime * bulletSpeed);
		}
	}

	void OnTriggerEnter2D (Collider2D coll) {
		ExplosiveMaterial m = coll.GetComponent<ExplosiveMaterial>();

		if(m != null){
			Instantiate(Registry.prefabSpawner.explosionPrefab, coll.transform.position, Quaternion.identity);
			Destroy(coll.gameObject);
			Destroy(gameObject);
		}
		
		Planet plan = coll.GetComponent<Planet> ();

		if (plan != null) {
			Instantiate(Registry.prefabSpawner.explosionPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
		}

        Bullet bul = coll.GetComponent<Bullet>();

        if (bul != null)
        {
            if (player != bul.player || player == false)
            {
                Instantiate(Registry.prefabSpawner.explosionPrefab, (transform.position + coll.transform.position) / 2, Quaternion.identity);
                Destroy(coll.gameObject);
                Destroy(gameObject);
            }
        }

		Mob mob = coll.GetComponent<Mob> ();

		if (coll.GetComponent<Collider2D>().tag == "Planet") {
			stopped = true;
		}

		if (mob != null) {
			if ((mob == Registry.player && !player) || (mob != Registry.player && player)) {
				mob.health -= damage;
				Destroy (gameObject);
			}
		}/* else {
			Pickup pickup = coll.GetComponent<Pickup> ();

			if (pickup == null) {
				Destroy (gameObject);
			}
		}*/
	}
}