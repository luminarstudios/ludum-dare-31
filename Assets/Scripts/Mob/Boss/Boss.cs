﻿using UnityEngine;
using System.Collections;

public class Boss : Mob {

	[HideInInspector]
	public GameObject bossPlatform;
	
	public float spawnEnemyTime = 5.0f;
	public float randomSpawnOffset = 2.5f;
	
	public GameObject laserTipPrefab;
	public float laserTime = 5.0f;
	public float laserTimeLength = 2.5f;
	public float randomLaserOffset = 2.5f;

	GameObject laserTip;

	float laserTimer = 0;
	float randLaserOffset = 0;

	float spawnTimer = 0;
	float randSpawnOffset = 0;

	LineRenderer laser;

	Vector3 nextLaserPos;

	void Start(){
		Registry.boss = this;
		GenerateNewLaserPos ();
		laser = GetComponent<LineRenderer> ();

		laserTip = (GameObject)Instantiate (laserTipPrefab, transform.position + new Vector3(0, 0, -transform.position.z + laserTipPrefab.transform.position.z), Quaternion.identity);

		randSpawnOffset = Random.Range (-randomSpawnOffset, randomSpawnOffset);
		randLaserOffset = Random.Range (-randomLaserOffset, randomLaserOffset);
	}

	void GenerateNewLaserPos(){
		nextLaserPos = new Vector3 (Random.Range(bossPlatform.transform.position.x - ((BoxCollider2D)bossPlatform.GetComponent<Collider2D>()).size.x / 2,
		                                         bossPlatform.transform.position.x + ((BoxCollider2D)bossPlatform.GetComponent<Collider2D>()).size.x / 2),
		                            Random.Range(bossPlatform.transform.position.y - ((BoxCollider2D)bossPlatform.GetComponent<Collider2D>()).size.y / 2,
		             							bossPlatform.transform.position.y + ((BoxCollider2D)bossPlatform.GetComponent<Collider2D>()).size.y / 2));
	}

	void Update(){
		if (health <= 0) {
			Die ();
		}

		if (laserTip.activeInHierarchy) {
			laser.enabled = true;
			laser.SetPosition (0, transform.position + new Vector3(0, 0, -transform.position.z + laserTip.transform.position.z));
			laser.SetPosition (1, laserTip.transform.position);
		} else {
			laser.enabled = false;
		}

		spawnTimer += Time.deltaTime;
		if (spawnTimer >= spawnEnemyTime + randSpawnOffset) {
			SpawnEnemy();
			spawnTimer = 0;
			randSpawnOffset = Random.Range(-randomSpawnOffset, randomSpawnOffset);
		}

		laserTimer += Time.deltaTime;
		if (laserTimer >= laserTime + randLaserOffset) {
			laserTip.SetActive (true);
			if (laserTimer <= laserTime + randLaserOffset + laserTimeLength) {
				laserTip.transform.position = Vector3.Lerp(laserTip.transform.position, nextLaserPos, (laserTimer - (laserTime + randLaserOffset)) / laserTimeLength);
			} else {
				laserTimer = 0;
				randLaserOffset = Random.Range (-randomLaserOffset, randomLaserOffset);
				GenerateNewLaserPos ();
			}
		} else {
			laserTip.SetActive(false);
		}
	}

	public void SpawnEnemy(){
		Vector3 randomPos = new Vector3 (Random.Range(bossPlatform.transform.position.x - ((BoxCollider2D)bossPlatform.GetComponent<Collider2D>()).size.x / 2 + 1.5f,
		                                              bossPlatform.transform.position.x + ((BoxCollider2D)bossPlatform.GetComponent<Collider2D>()).size.x / 2 - 1.5f),
		                            	 Random.Range(bossPlatform.transform.position.y - ((BoxCollider2D)bossPlatform.GetComponent<Collider2D>()).size.y / 2 + 1.5f,
		             								  bossPlatform.transform.position.y + ((BoxCollider2D)bossPlatform.GetComponent<Collider2D>()).size.y / 2 - 1.5f));
		Instantiate (Registry.mainGame.levelData[Registry.currentLevel].enemyPrefabs[Random.Range(0, Registry.mainGame.levelData[Registry.currentLevel].enemyPrefabs.Length)], randomPos, Quaternion.identity);
	}

	public void Die(){
		Instantiate (Registry.prefabSpawner.blackHolePrefab, transform.position, Registry.prefabSpawner.blackHolePrefab.transform.rotation);
		bossPlatform.GetComponent<BossPlatform> ().Unlock ();
		Destroy (laserTip);
		Destroy (gameObject);
	}

	void OnTriggerEnter2D(Collider2D coll){
		Bullet b = coll.GetComponent<Bullet> ();

		if (b != null) {
			if(b.player){
				health -= b.damage;
			}
		}
	}

}
